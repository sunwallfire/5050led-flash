import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

const _httpOptions = new HttpHeaders({
  'Content-Type':  'application/json',
  'Authorization': 'my-auth-token'
})


const postUrl = "http://24.136.22.177:92/flash"//"http://127.0.0.1:3000/test";

@Injectable()
export class ControlService {

  constructor(private http: HttpClient) { }

  public flashLove(reloadTime: any){
    this.sendFlashValue(1, reloadTime);
  }

  public flashFlicker(reloadTime: any){
    this.sendFlashValue(2, reloadTime);
  }

  public flashPoke(reloadTime: any){
    console.log(this.sendFlashValue(3, reloadTime));
  }

  public flashKill(reloadTime: any){
    this.sendFlashValue(4, reloadTime);
  }

  private sendFlashValue(op : any, reloadTime : any){
    let subscribe = this.http.post(postUrl, {
      option: 'response'
    }, 
    {
      headers: _httpOptions,
      responseType: 'text' 
    })
    .subscribe((res) => {
      if(res == "OK"){
        setInterval(() => {
          subscribe.unsubscribe();
        }, reloadTime);
      }
      return res;
    })
    /*.subscribe(
      (val) => {
        console.log("POST call successful value returned in body", val);
      },
      response => {
          console.log("POST response", response);
      },
      () => {
          console.log("The POST observable is now completed.");
      });*/
  }

}
