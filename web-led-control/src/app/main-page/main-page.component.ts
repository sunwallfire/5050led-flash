import { Component, OnInit } from '@angular/core';
import { ControlService } from './services/control.service';

@Component({
  selector: 'app-main-page',
  providers: [ControlService],
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  isFlashingLight: any;
  poke_timer: any;

  constructor(private controlService: ControlService) {  }

  ngOnInit() {
    this.isFlashingLight  = false;
    this.poke_timer = null;

    var element = document.getElementById("poke-container");
    element.addEventListener("click", function(e){
      e.preventDefault;
      element.classList.remove("beep-boop-blink");
      void element.offsetWidth;
      element.classList.add("beep-boop-blink");
    }, false);
  }

  heart_click(event : any){
    if(this.isFlashingLight) return;
    this.isFlashingLight = true;
    event.target.classList.add("heart-blink");
    setInterval(() => {
      event.target.classList.remove("heart-blink");
      this.isFlashingLight = false;
      window.location.reload();
    },10000);
    this.controlService.flashLove(10000);
  }

  //don't need to worry about clicking other events since this one happens so fast
  beep_boop_click(event : any){
    this.controlService.flashPoke(1000);
  }

  kill_click(event : any){
    if(this.isFlashingLight) return;
    this.isFlashingLight = true;
    event.target.classList.add("kill-blink");
    setInterval(() => {
      event.target.classList.remove("kill-blink");
      this.isFlashingLight = false;
      window.location.reload();
    },5000);
    this.controlService.flashKill(5000);
  }

}
