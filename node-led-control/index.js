#!/usr/bin/env node
var compression = require('compression');//compress file to make it even faster
var helmet = require('helmet');//protect against known http vulnerabilities
var http = require('http');
var fs = require('fs');
var connect = require('connect');
var express = require('express');
var cors = require('cors')
var mysql = require('mysql');
var url = require('url');
var serial = require('serial');//for arduino
var bodyParser = require('body-parser');//for post requests
var SerialPort = require('serialport');
const struct = require('python-struct');

var arduino = '/dev/serial/by-id/usb-Arduino__www.arduino.cc__0042_55739323031351410152-if00';
var port = new SerialPort(arduino, { baudRate: 9600 }, function(err){
    if(err){
        return console.log('Error: ', err.message);
    }
});



var app = express();
app.use(cors());
app.use(bodyParser.json());//to support JSON-encoded bodies
app.use(bodyParser.urlencoded({//to support URL-encoded bodies
    extended: false
}));
app.use(compression());
app.use(helmet());

app.options('*', cors()) // include before other routes; enabling CORS pre-flight


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Expose-Headers");
    next();
});

var createConnection = () => {
    /*return new mysql.createConnection({
        host     : "healthcare-db.cflaxn8luuia.us-east-1.rds.amazonaws.com",
        user     : "healthcare",
        password : "healthcareproduction",
        port     : "3306",
        database : "healthcare_db"
    });*/
}

app.get('/', function(req, res) {
    res.send('Hello World');
});

app.post('/test', function(req, res, next){
    var result = req.body.option;
    res.status(200).send("OK");
});

app.post('/flash', function(req, res, next) {
    //res.writeHead(200, {'Content-Type': 'application/json'});
    port.write(struct.pack('>B', req.body.option));
    res.status(200).send("OK");
});

var server = app.listen(3000, function(){
    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at ::%s", port);
});