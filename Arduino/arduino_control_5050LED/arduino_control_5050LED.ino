// color swirl! connect an RGB LED to the PWM pins as indicated
// in the #defines
// public domain, enjoy!
 
#define REDPIN 5
#define GREENPIN 6
#define BLUEPIN 3
#define WHITE 

#define REDPIN2 8
#define GREENPIN2 9
#define BLUEPIN2 10
 
void setup() {
  Serial.begin(9600);
  while(!Serial){
    Serial.println("Waiting for connection...");
  }
  pinMode(REDPIN, OUTPUT);
  pinMode(GREENPIN, OUTPUT);
  pinMode(BLUEPIN, OUTPUT);

  pinMode(REDPIN2, OUTPUT);
  pinMode(GREENPIN2, OUTPUT);
  pinMode(BLUEPIN2, OUTPUT);
}
 
 
void loop() {
  setColor(0,0,0);
  int state = 0;
  if(Serial.available() > 0){
    state = Serial.read();
    Serial.println(state, DEC);
    if(state == 1){
      love_me();
    }
    else if(state == 2){
      flicker_me();
    }
    else if(state == 3){
      poke_me();
    }
    else if(state == 4){
      kill_me();
    }
  }
}

void love_me(){
  for(int i = 0; i < 3; ++i){
    delay(1000);
    for(int j = 0; j < 256; ++j){
      setColor(j, 0, 0);
      delay(10);
    }
    for(int j = 255; j >= 0; --j){
      setColor(j, 0, 0);
      delay(10);
    } 
  }
}

void flicker_me(){
  for(int i = 0; i < 3; ++i){
    setColor(50, 50, 50);
    delay(100);
    setColor(0,0,0);
    delay(50);
    
    setColor(50, 50, 50);
    delay(100);
    setColor(0,0,0);
    delay(200);
    
    for(int j = 255; j >= 0; j--){
      setColor(j, j, j);
      delay(10);
    }
    delay(1500);
  }
}

void poke_me(){
  setColor(100, 100, 100);
  delay(300);
  setColor(0,0,0);
}

void kill_me(){
  int number_times_flash = 50;
  int flash_speed = 50;
  randomColorFlashing(number_times_flash, flash_speed);
}

void setColor(int red, int green, int blue){
  red = 255 - red;
  green = 255 - green;
  blue = 255 - blue;
  red = red < 0 ? 0 : red;
  green = green < 0 ? 0 : green;
  blue = blue < 0 ? 0 : blue;
  analogWrite(REDPIN, red);
  analogWrite(GREENPIN, green);
  analogWrite(BLUEPIN, blue);

  analogWrite(REDPIN2, red);
  analogWrite(GREENPIN2, green);
  analogWrite(BLUEPIN2, blue);
}

void randomColor(){
  int red = random(2)*255;
  int green = random(2)*255;
  int blue = random(2)*255;
  while(red == 0 && green == 0 && blue == 0){
    red = random(2)*255;
    green = random(2)*255;
    blue = random(2)*255;
  }
  setColor(red,green,blue);
}

void randomColorFlashing(int max, int speed){
  //speed is in milliseconds
  for(int i = 0; i < max; ++i){
    delay(speed);
    randomColor();
    delay(speed);
    setColor(0,0,0);
  }
}
